// Importando as bibliotecas e componentes necessários do React Native
import React, { useState } from "react";
import { View, ScrollView, Image, Text, StyleSheet, Button, Dimensions, TouchableOpacity, Modal } from "react-native";
import { AntDesign } from "@expo/vector-icons"; 

// Importando as imagens usando require do Expo
const futebolImage = require("../assets/futebol.png");
const basqueteImage = require("../assets/basquete.png");
const tenisImage = require("../assets/tenis.png");
const voleiImage = require("../assets/volei.png");


const quadrasInfo = [
  {
    name: "Quadra de Futebol",
    image: futebolImage,
    description: "Quadra de futebol society, com dimensões padrão, gramado sintético de alta qualidade e iluminação para jogos noturnos.",
  },
  {
    name: "Quadra de Basquete",
    image: basqueteImage,
    description: "Quadra de basquete com piso de madeira de alta qualidade, cestas padrão e iluminação para jogos noturnos.",
  },
  {
    name: "Quadra de Tênis",
    image: tenisImage,
    description: "Quadra de tênis com piso de saibro, equipada com rede e postes de qualidade profissional.",
  },
  {
    name: "Quadra de Vôlei",
    image: voleiImage,
    description: "Quadra de vôlei de areia, com rede profissional e espaço amplo para jogos.",
  },
];

// Definindo o componente HomeScreen
export default function HomeScreen({ navigation }) {
  // Definição de estados
  const [cont, setCount] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [selectedQuadra, setSelectedQuadra] = useState({});
  const screenWidth = Dimensions.get("window").width;
  const imageWidth = screenWidth - 40; // Largura da imagem com um espaço de 20 em cada lado
  const imageHeight = 240; // Altura da imagem

  // Função para lidar com o scroll horizontal
  const handleScroll = (event) => {
    const contentOffsetX = event.nativeEvent.contentOffset.x;
    const currentIndex = Math.floor(contentOffsetX / imageWidth);
    setCount(currentIndex);
  };

  // Função para abrir o modal com informações da quadra selecionada
  const openModal = (index) => {
    setModalVisible(true);
    setSelectedQuadra(quadrasInfo[index]);
  };

  // Função para lidar com a reserva da quadra
  const handleReservation = () => {
    // Aqui você pode adicionar a lógica para reservar a quadra
    // Por exemplo, você pode navegar para uma tela de reserva ou realizar uma ação específica
    // Neste exemplo, estou apenas exibindo um alerta para fins de demonstração
    alert("Sua reserva foi feita com sucesso!");
    // Fechar o modal após a reserva
    setModalVisible(false);
  };

  // Renderização da interface
  return (
    <View style={styles.container}>
      {/* Botão de voltar */}
      <TouchableOpacity style={styles.backButton} onPress={() => navigation.navigate("PaginaInicial")}>
        <AntDesign name="arrowleft" size={24} color="#3e945a" />
      </TouchableOpacity>
      {/* Conteúdo principal */}
      <View style={styles.contentContainer}>
        {/* Título */}
        <Text style={styles.title}>BEM-VINDO À NOSSA APLICAÇÃO</Text>
        {/* Subtítulo */}
        <Text style={styles.subtitle}>Conheça algumas de nossas quadras!</Text>
        {/* ScrollView horizontal para exibir as quadras */}
        <ScrollView
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
          onScroll={handleScroll}
          scrollEventThrottle={16}
          contentContainerStyle={styles.scrollViewContent}
        >
          {/* Renderização das imagens das quadras */}
          {quadrasInfo.map((quadra, index) => (
            <TouchableOpacity key={index} onPress={() => openModal(index)} style={styles.imageContainer}>
              <Image source={quadra.image} style={{ ...styles.image, width: imageWidth, height: imageHeight }} />
              <Text style={styles.description}>{quadra.name}</Text>
            </TouchableOpacity>
          ))}
        </ScrollView>
      </View>

      {/* Modal */}
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => setModalVisible(false)}
      >
        <View style={styles.modalContainer}>
          <View style={styles.modalContent}>
            <Image source={selectedQuadra.image} style={{ ...styles.modalImage, width: imageWidth, height: imageHeight }} />
            <Text style={styles.modalName}>{selectedQuadra.name}</Text>
            <Text style={styles.modalDescription}>{selectedQuadra.description}</Text>
            {/* Botão para reservar a quadra */}
            <TouchableOpacity onPress={handleReservation} style={styles.modalButton}>
            <Button title="RESERVAR" onPress={() => setModalVisible(false)} />
            </TouchableOpacity>
            {/* Botão para fechar o modal */}
            <View style={styles.modalButton2}>
              <Button title="Fechar" onPress={() => setModalVisible(false)} />
            </View>
            
          </View>
        </View>
      </Modal>

      {/* Botão de reserva */}
      <View style={styles.buttonContainer}>
        <View style={styles.button}>
          <Button
            title="RESERVE AQUI"
            onPress={() => navigation.navigate("Reservas")}
            color="#3e945a"
          />
        </View>
      </View>

      {/* Rodapé */}
      <View style={styles.footer}>
        <Text style={styles.footerText}>© 2024 Reserva de Quadras Esportivas</Text>
      </View>
    </View>
  );
}

// Estilos para os componentes
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#f0f0f0",
    alignItems: "center",
    justifyContent: "center",
  },
  contentContainer: {
    marginTop: 100, // Ajusta a margem superior para o conteúdo principal
  },
  title: {
    fontSize: 16,
    fontWeight: "bold",
    marginBottom: 10,
    textAlign: 'center', // centralizar o texto
  },
  subtitle: {
    fontSize: 16,
    marginBottom: 10, // Ajusta a margem inferior
    textAlign: 'center', // centralizar o texto
  },
  scrollViewContent: {
    flexGrow: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  imageContainer: {
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 20,
    marginBottom: 20,
  },
  image: {
    resizeMode: "contain",
    marginVertical: -85,
  },
  description: {
    fontSize: 18,
    fontWeight: "bold",
    marginVertical: 90, // Aumentei a margem vertical da imagem
    textAlign: "center",
  },
  modalContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(0, 0, 0, 0.5)", // Adiciona um fundo escuro transparente
  },
  modalContent: {
    backgroundColor: "white",
    borderRadius: 20,
    padding: 20,
    alignItems: "center",
  },
  modalImage: {
    resizeMode: "contain",
    width: "100%",
    height: 200,
    marginBottom: 10,
  },
  modalName: {
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
    textAlign: "center",
  },
  modalDescription: {
    fontSize: 16,
    marginBottom: 10,
    textAlign: "center",
  },
  modalFacilities: {
    fontSize: 14,
    textAlign: "center",
  },
  buttonContainer: {
    alignItems: "center",
    justifyContent: "center",
    marginTop: -50, // Ajusta a margem superior negativamente
    marginBottom: 50,
  },
  modalButton: {
    width: "60%",
    borderRadius: 20,
    marginVertical: 2, // Aumentei a margem vertical
    overflow: 'hidden',
    backgroundColor: '#e6e6e6',
  },
  modalButton2: {
    width: "60%",
    borderRadius: 20,
    marginVertical: 2, // Aumentei a margem vertical
    overflow: 'hidden',
    backgroundColor: '#e6e6e6',
    width: 94,
  },
  button: {
    width: "60%",
    borderRadius: 20,
    marginVertical: 2, // Aumentei a margem vertical
    overflow: 'hidden',
    backgroundColor: '#e6e6e6',
    position: "absolute",
    top: -110,
    left: -150,
  },
  footer: {
    position: "absolute",
    bottom: 0,
  },
  footerText: {
    fontSize: 14,
    color: "#666",
  },
  backButton: {
    position: "absolute",
    top: 40,
    left: 20,
    zIndex: 999,
  },
  buttonText: {
    color: "#fff",
    textAlign: "center",
    fontSize: 16,
  },
});
