import React from 'react';
import { StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import PaginaInicial from './src/paginaInicial';
import LoginScreen from './src/loginScreen';
import RegisterScreen from './src/registerScreen';
import HomeScreen from './src/homeScreen';
import ReservasScreen from './src/reservasScreen';
import ConexaoDb from './src/conexaoDb';


const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{ headerShown: false }} >
        <Stack.Screen name='PaginaInicial' component={PaginaInicial} />
        <Stack.Screen name='Login' component={LoginScreen} />
        <Stack.Screen name='Conexao' component={ConexaoDb} />
        <Stack.Screen name='Home' component={HomeScreen} />
        <Stack.Screen name='Reservas' component={ReservasScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});