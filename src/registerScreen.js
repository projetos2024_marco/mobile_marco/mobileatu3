// import React, { useState } from "react";
// import { View, TextInput, Button, StyleSheet, Text, Modal, TouchableOpacity } from "react-native";
// import { AntDesign } from "@expo/vector-icons"; // Importe o ícone de seta da biblioteca expo
// import users from './api';


// const RegisterScreen = ({ navigation }) => {
//   const [nome, setNome] = useState('');
//   const [email, setEmail] = useState('');
//   const [telefone, setTelefone] = useState('');
//   const [senha, setSenha] = useState('');
//   const [modalVisible, setModalVisible] = useState(false);
//   const [modalTitle, setModalTitle] = useState('');
//   const [modalMessage, setModalMessage] = useState('');

//   const showAlert = (title, message) => {
//     setModalTitle(title);
//     setModalMessage(message);
//     setTimeout(() => setModalVisible(true), 100);
//   };

//   const handleRegister = async () => {
//     try {
//       if (!nome || !senha || !email || !telefone) {
//         showAlert('Erro', 'Campos obrigatórios não preenchidos');
//         return;
//       }

//       if (senha.length < 7) {
//         showAlert('Erro', 'Sua senha precisa ter no mínimo 8 caracteres.');
//         return;
//       }

//       const simbolosEmail = /@/;

//       if (!simbolosEmail.test(email)) {
//         showAlert('Erro', 'Email inválido.');
//         return;
//       }

//       // Validar se o email possui um dos domínios específicos (.com, .io, .net, .br)
//       const dominiosPermitidos = /\.(com|io|net|br)$/i;
//       if (!dominiosPermitidos.test(email)) {
//         showAlert('Erro', 'O email deve ter um dos domínios permitidos: .com, .io, .net, .br');
//         return;
//       }

//       // Validar se o telefone contém apenas números
//       const telefoneNumerico = /^[0-9]+$/;
//       if (!telefoneNumerico.test(telefone)) {
//         showAlert('Erro', 'O telefone deve conter apenas números.');
//         return;
//       }

//       // Validar o comprimento máximo do telefone
//       const tamanhoMaximoTelefone = 15; // Altere conforme necessário
//       if (telefone.length > tamanhoMaximoTelefone) {
//         showAlert('Erro', `O telefone deve ter no máximo ${tamanhoMaximoTelefone} caracteres.`);
//         return;
//       }

//       const response = await users.postUser({ nome, email, telefone, senha });
      
//       if (response.status === 200) {
//         // Se o registro for bem-sucedido, navegue para a tela Home
//         showAlert('Sucesso', 'Usuário cadastrado com sucesso.');
//         navigation.navigate('Home');
//       } else {
//         // Se ocorrer um erro, exiba uma mensagem de erro
//         showAlert('Erro', response.data.message);
//       }
//     } catch (error) {
//       console.error('Erro ao cadastrar:', error);
//       showAlert('Erro', 'Algo deu errado. Tente novamente mais tarde.');
//     }
//   };

//   return (
//     <View style={styles.container}>
//       <Modal
//         animationType="slide"
//         transparent={true}
//         visible={modalVisible}
//         onRequestClose={() => {
//           setModalVisible(!modalVisible);
//         }}
//       >
//         <View style={styles.centeredView}>
//           <View style={styles.modalView}>
//             <Text style={styles.modalTitle}>{modalTitle}</Text>
//             <Text style={styles.modalText}>{modalMessage}</Text>
//             <Button
//               onPress={() => setModalVisible(!modalVisible)}
//               title="OK"
//               color="#3e945a"
//             />
//           </View>
//         </View>
//       </Modal>
//       <View>
//         <Text style={styles.title}>INSIRA SUAS INFORMAÇÕES DE</Text>
//         <Text style={[styles.title, styles.subtitle]}>CADASTRO</Text>
//       </View>
//       <TextInput
//         style={styles.input}
//         placeholder="Nome"
//         onChangeText={setNome}
//         value={nome}
//       />
//       <TextInput
//         style={styles.input}
//         placeholder="Email"
//         onChangeText={setEmail}
//         value={email}
//       />
//       <TextInput
//         style={styles.input}
//         placeholder="Telefone"
//         onChangeText={setTelefone}
//         value={telefone}
//       />
//       <TextInput
//         style={styles.input}
//         placeholder="Senha"
//         secureTextEntry={true}
//         onChangeText={setSenha}
//         value={senha}
//       />
//       <View style={styles.buttonContainer}>
//         <Button
//           title="CADASTRAR"
//           onPress={handleRegister}
//           color='#3e945a'
//         />
//       </View>

//       {/* Botão para voltar */}
//       <TouchableOpacity style={styles.backButton} onPress={() => navigation.navigate("PaginaInicial")}>
//         <AntDesign name="arrowleft" size={24} color="#3e945a" />
//       </TouchableOpacity>

//       <View style={styles.footer}>
//         <Text style={styles.footerText}>© 2024 Reserva de Quadras Esportivas</Text>
//       </View>
//     </View>
//   );
// };

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#FFFFFF', // Alterado para branco
//     alignItems: 'center',
//     justifyContent: 'center',
//     paddingHorizontal: 16,
//   },
//   title: {
//     textAlign: 'center',
//     fontSize: 16,
//     fontWeight: 'bold',
//     marginBottom: 1,
//   },
//   subtitle: {
//     textAlign: 'center',
//     fontSize: 16,
//     fontWeight: 'bold',
//   },
//   input: {
//     borderColor: '#4dbf72',
//     borderWidth: 3,
//     borderRadius: 30,
//     fontSize: 12,
//     width: '80%',
//     padding: 10,
//     marginVertical: 3,
//     marginTop: 3,
//     marginBottom: 5,
//     paddingHorizontal: 10,
//     height: 40,
//   },
//   buttonContainer: {
//     backgroundColor: '#e6e6e6',
//     borderRadius: 20,
//     overflow: 'hidden',
//     width: '80%', // para reduzir o tamanho
//     marginBottom: 3,
//     marginVertical: 15,
//     marginTop: 4,
//   },
//   goBackContainer: {
//     backgroundColor: '#e6e6e6',
//     borderRadius: 20,
//     overflow: 'hidden',
//     width: '80%',
//     marginBottom: 10,
//   },
//   footer: {
//     position: 'absolute',
//     bottom: 0,
//   },
//   footerText: {
//     fontSize: 14,
//     color: '#666',
//   },
//   centeredView: {
//     flex: 1,
//     justifyContent: "center",
//     alignItems: "center",
//     marginTop: 22
//   },
//   modalView: {
//     margin: 20,
//     backgroundColor: "white",
//     borderRadius: 20,
//     padding: 35,
//     alignItems: "center",
//     shadowColor: "#000",
//     shadowOffset: {
//       width: 0,
//       height: 2
//     },
//     shadowOpacity: 0.25,
//     shadowRadius: 4,
//     elevation: 5
//   },
//   modalTitle: {
//     marginBottom: 15,
//     textAlign: "center",
//     fontWeight: "bold",
//     fontSize: 20,
//     color: "#3e945a"
//   },
//   modalText: {
//     marginBottom: 15,
//     textAlign: "center",
//     fontSize: 16,
//   },
//   backButton: {
//     position: "absolute",
//     top: 40,
//     left: 20,
//     zIndex: 999,
//   },
// });

// export default RegisterScreen;
